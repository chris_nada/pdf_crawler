#!/usr/bin/env python
# coding=utf-8
import tkinter.filedialog
import tkinter.scrolledtext

import os
from tika import parser

n_chars   = 255
pdf_texts = {}
nachricht = ""

# Liefert gesamten Text einer PDF Datei
def get_pdf_text(pdf_file):
        roh = parser.from_file(pdf_file)
        roh = roh["content"]
        roh = roh.replace("\n", "")
        roh = roh.replace("\r", "")
        roh = roh.replace("\t", "")
        roh = roh.replace("\b", "")
        return str(roh)

# Ordner abfragen
win = tkinter.Tk()
frame = tkinter.Frame(master = win)
frame.pack(fill = tkinter.BOTH, expand = True)
pfad = tkinter.filedialog.askdirectory()
if len(pfad) < 4:
        exit(0)

for root, dirs, files in os.walk(pfad + "/"):
        for datei in files:
                datei = root + os.sep + datei
                if datei.lower().endswith((".pdf")):
                        try :
                                text = get_pdf_text(datei)
                                pdf_texts.update( {datei : text[:n_chars]} )
                        except Exception as e:
                                print(e)

iterated = []
for (datei1, text1) in pdf_texts.items():
    iterated.append(datei1)
    for (datei2, text2) in pdf_texts.items():
        if datei2 not in iterated and datei1 != datei2 and text1 == text2:
            nachricht += "Duplikat: " + datei1 + " &\n"
            nachricht += "          " + datei2 +   "\n"
if len(nachricht) == 0:
    nachricht += "No duplicates found."

# Text Widget
text = tkinter.scrolledtext.Text(master = frame, wrap = tkinter.WORD)
text.grid(row = 0, column = 0, sticky = tkinter.NSEW)
tkinter.Grid.rowconfigure   (frame, 0, weight = 1)
tkinter.Grid.columnconfigure(frame, 0, weight = 1)
text.insert(tkinter.INSERT, nachricht)

# Scrollbalken
scrollbar = tkinter.Scrollbar(master = frame, command = text.yview)
scrollbar.grid(row = 0, column = 1, sticky = tkinter.NSEW)
text["yscrollcommand"] = scrollbar.set

win.mainloop()