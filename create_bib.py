#!/usr/bin/env python
# coding=utf-8
import os, subprocess, string
import tkinter
import tkinter.scrolledtext
import tkinter.filedialog
from tika import parser

# Liefert gesamten Text einer PDF Datei
def get_pdf_text(pdf_file):
    roh = parser.from_file(pdf_file)
    roh = roh["content"]
    roh = roh.replace("\n", "")
    roh = roh.replace("\r", "")
    roh = roh.replace("\t", "")
    roh = roh.replace("\b", "")
    return str(roh)

# Ordner abfragen
win = tkinter.Tk()
frame = tkinter.Frame(master = win)
frame.pack(fill = tkinter.BOTH, expand = True)
pfad = tkinter.filedialog.askdirectory()

# Output erstellen
out_txt = "bibtex.txt"
out = open(out_txt, mode="w")

# Alle Dateien durchforsten
for root, dirs, files in os.walk(pfad + "/"):
        for datei in files:
                datei_pfad = root + os.sep + datei
                if datei_pfad.lower().endswith((".pdf")):
                    # Dateinamen anpassen
                    datei = str(datei_pfad)
                    datei = datei.strip().lower()
                    try: # Unterordner => <subdir>:<titel>
                        datei = datei[1 + datei[:datei.rfind(os.sep)].rfind(os.sep):]
                        datei = datei.replace(os.sep, ":")
                    except:
                        pass
                    datei = datei.replace("-", "_")
                    datei = datei.replace(" ", "_")
                    datei = "".join([c if ord(c) < 128 and c not in ",.'\"#=!?§$%&/()\\[]\{\}" else '' for c in datei])

                    # Automatisch auslesen
                    #inhalt = ""
                    #try:
                    #    inhalt = get_pdf_text(datei_pfad)
                    #except:
                    #    pass

                    # Eintrag schreiben
                    out.write("@book{")
                    out.write(datei[:-3]) # .pdf abschneiden
                    out.write(",\n")
                    out.write("    author = \"\",\n")
                    out.write("    title = \"\",\n")
                    #out.write("    address = \"\",\n")
                    out.write("    publisher = \"\",\n")
                    out.write("    year = \"\"\n}\n\n")
                    

# Datei im Editor öffnen                
out.close()
out_txt = os.getcwd() + os.sep + out_txt
print (out_txt)
subprocess.call(out_txt, shell=True)
