#!/usr/bin/env python
# coding=utf-8
import pymsgbox, os
import tkinter
import tkinter.scrolledtext
import tkinter.filedialog
from tika import parser

#pfad  = "C:/users/krizchri/Desktop/papers"
suche = "army"

# Liefert gesamten Text einer PDF Datei
def get_pdf_text(pdf_file):
        roh = parser.from_file(pdf_file)
        roh = roh["content"]
        roh = roh.replace("\n", "")
        roh = roh.replace("\r", "")
        roh = roh.replace("\t", "")
        roh = roh.replace("\b", "")
        return str(roh)

# Liefert alle Sätze, die einen der Suchbegriffe enthalten
def get_saetze(keys, text):
        saetze = []
        for satz in text.lower().split(". "):
                for key in keys:
                        if key in satz:
                                saetze.append(str(satz))
                                break
        return saetze

# Enthält der Text alle gegebenen Suchbegriffe?
def search_keys(keys, text):
        text = text.lower()
        for key in keys:
                if key not in text:
                        return False
        return True

# Ordner abfragen
win = tkinter.Tk()
frame = tkinter.Frame(master = win)
frame.pack(fill = tkinter.BOTH, expand = True)
pfad = tkinter.filedialog.askdirectory()

# Suchbegriffe abfragen
suche = pymsgbox.prompt("Text", "Titel")
suche = suche.split(" ")

nachricht: str = ""
for root, dirs, files in os.walk(pfad + "/"):
        for datei in files:
                datei = root + os.sep + datei
                if datei.lower().endswith((".pdf")):
                        try :
                                text = get_pdf_text(datei)
                                saetze = get_saetze(suche, text)
                                if len(saetze) > 0:
                                        nachricht += ("\n# SUCHERGEBNIS: " + datei + ":\n\n")
                                        zaehler = 1
                                        for satz in saetze:
                                                if len(satz) > 100:
                                                        satz = satz[:100] + " [...]"
                                                nachricht += ("\t" + str(zaehler) + ". " + satz + "\n")
                                                zaehler += 1
                        except Exception as e:
                                print("Fehler: " + str(e))

# Text Widget
text = tkinter.scrolledtext.Text(master = frame, wrap = tkinter.WORD)
text.grid(row = 0, column = 0, sticky = tkinter.NSEW)
tkinter.Grid.rowconfigure   (frame, 0, weight = 1)
tkinter.Grid.columnconfigure(frame, 0, weight = 1)
text.insert(tkinter.INSERT, nachricht)
text.tag_config("mark", background = "yellow")

# Scrollbalken
scrollbar = tkinter.Scrollbar(master = frame, command = text.yview)
scrollbar.grid(row = 0, column = 1, sticky = tkinter.NSEW)
text["yscrollcommand"] = scrollbar.set

# Suche markieren
for begriff in suche:
        pos = "1.0"
        while True:
                idx = text.search(begriff, pos, tkinter.END)
                if not idx:
                        break
                pos = "{}+{}c".format(idx, len(begriff))
                text.tag_add("mark", idx, pos)

win.mainloop()
