### pdf_crawl.py
*Searches all PDFs in given folder and subfolders for given terms*
+ Run pdf_crawl.py
+ Choose folder: All PDFs in this folder and subfolders will be searched.
+ Input search terms. You can input multiple terms, seperated by blanks.
  
![](https://gitlab.com/chris_nada/pdf_crawler/wikis/uploads/2a33ea681868364ffd1d7890db2b0cf0/image.png)

+ You will then see an overview with the results.

![](https://gitlab.com/chris_nada/pdf_crawler/wikis/uploads/1c5d9e8b6f74b059dab656782ae9588b/image.png)

### pdf_duplicate.py
*Searches given folder and all of its subfolders for duplicate PDFs*
+ Run pdf_duplicate.py
+ Choose folder: All PDFs in this folder and subfolders will be searched.
+ You will then see an overview with the results.

### create_bib.py
*Searches given folder and all of its subfolders for PDFs and creates a file 'bibtex.txt' containing entries for each PDF*
+ Run pdf_duplicate.py
+ Choose folder: All PDFs in this folder and subfolders will be given an entry like so:
```
@book{<FILENAME>,
    author = "",
    title = "",
    publisher = "",
    year = ""
}
```
+ Note: <FILENAME> will be stripped of all characters not allowed for bibtex entries.

### Requirements
+ Simply run script with Python (Python 3 required).
+ Dependencies: pymsgbox and tika.
  To install, type `pip install tika` and `pip install pymsgbox` and run in console window.